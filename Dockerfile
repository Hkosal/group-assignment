#! PHP >=7.1
#! Database Postgres
#! Webserver nginx

FROM php:7.3-fpm-alpine3.11

# Composer Version
ENV ComposerVersion 1.10.6

# RUN install packages required
RUN set -ex && \
    apk --no-cache add \
        postgresql-dev

# Install PHP extensions
RUN docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql && \
    docker-php-ext-install pdo pdo_pgsql pdo_mysql

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin \
    --filename=composer --version=${ComposerVersion}

RUN echo 'memory_limit = -1' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini

# Set working directory
WORKDIR /var/www/html

COPY . /var/www/html

# Run composer install
RUN composer install \
    --ignore-platform-reqs \
    --no-scripts \
    --no-dev

# Change ownership
# RUN mkdir -p cache && \
RUN chgrp -R www-data storage && \
    chmod -R 775 storage
